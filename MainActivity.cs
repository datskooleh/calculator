﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Reflection;

namespace Calculator
{
	[Activity (Label = "Super REAL calc!!!",
	           MainLauncher = true,
			   Icon = "@drawable/icon",
			   ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{
		EditText ExpressionInput;

		Dictionary<int, Button> ButtonsNumbers = new Dictionary<int, Button>();
		Dictionary<int, Button> ButtonsOperations = new Dictionary<int, Button>();
		Dictionary<string, OperationDelegate> Operations = new Dictionary<string, OperationDelegate>();

		Button DecimalPoint;
		Button ButtonEvaluation;

		Button ButtonRemoveAll;
		Button ButtonRemoveLast;

		Button ButtonRevert;

		String leftExpression = "0", rightExpression;

		private delegate string OperationDelegate(double a, double b);
		private OperationDelegate Operation;

		Boolean restartCalculation = false;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			var fields = typeof(Resource.Id).GetFields ();

			GetNumericButtons (fields);
			GetOperationButtons(fields);

			var restFields = typeof(Resource.Id).GetFields ().Where(rest => fields.Any(field => field.Name == rest.Name)).ToList();

			ExpressionInput = FindViewById<EditText> (Resource.Id.Expression);
			ExpressionInput.Text = leftExpression;

			DecimalPoint = FindViewById<Button> (Resource.Id.ButtonDecimalPoint);
			DecimalPoint.Text = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
			DecimalPoint.Click += DecimalPointButtonClicked;
			
			ButtonEvaluation = FindViewById<Button> (Resource.Id.ButtonEvaluate);
			ButtonEvaluation.Click += EvaluationButtonClicked;

			ButtonRemoveAll = FindViewById<Button> (Resource.Id.ButtonRemoveAll);
			ButtonRemoveAll.Click += RemoveAllButtonClicked;

			ButtonRemoveLast = FindViewById<Button> (Resource.Id.ButtonRemoveLast);
			ButtonRemoveLast.Click += RemoveLastButtonClicked;

			ButtonRevert = FindViewById<Button> (Resource.Id.ButtonOperationRevert);
			ButtonRevert.Click += RevertButtonClicked;
		}

		private void NumberButtonClicked(object sender, EventArgs args)
		{
			if (ExpressionInput.Text.Length > 20)
				return;
			if (restartCalculation)
			{
				ExpressionInput.Text = String.Empty;
				restartCalculation = false;
			}

			Button clickedButton = sender as Button;

			if (clickedButton == null)
				return;

			if (Operation == null)
				Calculator.Helper.Main.AddOrReplace (ref leftExpression, clickedButton.Text);
			else
				Calculator.Helper.Main.AddOrReplace (ref rightExpression, clickedButton.Text);

			var exprInpt = ExpressionInput.Text;
			Calculator.Helper.Main.AddOrReplace (ref exprInpt, clickedButton.Text);
			ExpressionInput.Text = exprInpt;
		}

		private void DecimalPointButtonClicked(object sender, EventArgs args)
		{
			restartCalculation = false;

			var clickedButton = sender as Button;

			if (Operation == null)
			{
				if(Calculator.Helper.Main.AssignIfNotDecimal(ref leftExpression, clickedButton.Text) == false)
					return;
			}
			else
			{
				if(Calculator.Helper.Main.AssignIfNotDecimal(ref rightExpression, clickedButton.Text) == false)
					return;
			}

			ExpressionInput.Text += clickedButton.Text;
		}

		private void EvaluationButtonClicked(object sender, EventArgs args)
		{
			if (Operation == null)
				return;
			
			string expressionResult = String.Empty;

			Double left = Convert.ToDouble (leftExpression),
			right = Convert.ToDouble (rightExpression);

			expressionResult = Operation (left, right);

			leftExpression = expressionResult;
			rightExpression = String.Empty;
			Operation = null;

			ExpressionInput.Text = expressionResult;

			restartCalculation = true;
		}

		private void OperationButtonClicked(object sender, EventArgs args)
		{
			if (Operation != null)
			{
				if (!String.IsNullOrWhiteSpace (rightExpression))
					EvaluationButtonClicked (sender, args);
				else
					ExpressionInput.Text = ExpressionInput.Text.Remove (ExpressionInput.Text.Length - 1, 1);
				Operation = null;
			}

			if (leftExpression.Length == leftExpression.IndexOf (".") + 1)
			{
				leftExpression = leftExpression.Remove (leftExpression.Length - 1, 1);
				ExpressionInput.Text = ExpressionInput.Text.Remove (ExpressionInput.Text.Length - 1, 1);
			}

			restartCalculation = false;

			Button clickedButton = sender as Button;

			if (clickedButton.Text == "+")
				Operation = Calculator.Operations.Arithmetic.Plus;
			else if (clickedButton.Text == "*")
				Operation = Calculator.Operations.Arithmetic.Mult;
			else if (clickedButton.Text == "-")
				Operation = Calculator.Operations.Arithmetic.Minus;
			else if (clickedButton.Text == "/")
				Operation = Calculator.Operations.Arithmetic.Div;

			ExpressionInput.Text += clickedButton.Text;
		}

		private void RemoveAllButtonClicked(object sender, EventArgs args)
		{
			leftExpression = "0";
			rightExpression = String.Empty;
			Operation = null;

			ExpressionInput.Text = "0";
		}

		private void RevertButtonClicked(object sender, EventArgs args)
		{
			if (String.IsNullOrWhiteSpace (ExpressionInput.Text))
				return;

			if (Operation == null)
			{
				if (ExpressionInput.Text [0] == '-')
				{
					ExpressionInput.Text = ExpressionInput.Text.Remove (0, 1);
					leftExpression = leftExpression.Remove (0, 1);
				}
				else
				{
					ExpressionInput.Text = ExpressionInput.Text.Insert (0, "-");
					leftExpression = leftExpression.Insert (0, "-");
				}
			}
		}

		private void RemoveLastButtonClicked(object sender, EventArgs args)
		{
			if (ExpressionInput.Text == "0")
				return;
			else if (ExpressionInput.Text.Length == 1)
			{
				leftExpression = "0";
				ExpressionInput.Text = "0";
				return;
			}

			ExpressionInput.Text = ExpressionInput.Text.Remove (ExpressionInput.Text.Length - 1, 1);

			if (Operation == null)
			{
				if (leftExpression.Length == 1)
				{
					leftExpression = "0";
					return;
				}

				leftExpression = leftExpression.Remove (leftExpression.Length - 1, 1);
			}
			else if (!String.IsNullOrWhiteSpace (rightExpression))
			{
				rightExpression = rightExpression.Remove (rightExpression.Length - 1, 1);
			}
			else
				Operation = null;
		}

		private void GetNumericButtons(FieldInfo[] fields)
		{
			var numericFields = fields.Where (x => x.Name.Contains ("ButtonNumber")).ToList();
			foreach (var field in numericFields)
			{
				var viewButton = FindViewById<Button> ((int)field.GetValue(this.Resources));
				viewButton.Click += NumberButtonClicked;
				ButtonsNumbers.Add(viewButton.Id, viewButton);
			}
		}

		private void GetOperationButtons(FieldInfo[] fields)
		{
			var operationFields = fields.Where (x => x.Name.Contains ("ButtonOperation")).ToList();
			foreach (var field in operationFields)
			{
				var viewButton = FindViewById<Button> ((int)field.GetValue(this.Resources));
				viewButton.Click += OperationButtonClicked;
				ButtonsOperations.Add(viewButton.Id, viewButton);
			}
		}
	}
}