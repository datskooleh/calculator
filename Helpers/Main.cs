﻿using System;

namespace Calculator.Helper
{
	public class Main
	{
		public static void AddOrReplace(ref string taget, string source)
		{
			if (taget == "0")
				taget = source;
			else
				taget += source;
		}

		public static bool HasFloatingPoint(string source)
		{
			return (source.Contains (",") || source.Contains ("."));
		}

		public static bool AssignIfNotDecimal(ref string target, string source)
		{
			if(String.IsNullOrWhiteSpace(target) || HasFloatingPoint(target) )
				return false;

			target += source;

			return true;
		}
	}
}

