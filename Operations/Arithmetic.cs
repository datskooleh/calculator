﻿using System;

namespace Calculator.Operations
{
	public class Arithmetic
	{

		public static string Plus(double a, double b)
		{
			return (a + b).ToString ();
		}

		public static string Minus(double a, double b)
		{
			return (a - b).ToString ();
		}

		public static string Mult(double a, double b)
		{
			return (a * b).ToString ();
		}

		public static string Div(double a, double b)
		{
			return (a / b).ToString ();
		}

		public static string Revert(double a)
		{
			return (a *= -1).ToString();
		}
	}
}

